package fr.afpa.constants;

public interface Constants {


		
		//Nom du jeu
		public static final String TITRE = "***Space_¤_Combat***";
		
		//Dimension de la fenêtre principale
		public static final int LARGEUR_FENETRE = 900;
		public static final int HAUTEUR_FENETRE = 750;
		
		// Dimension du vaisseau
		public static final int LARGEUR_SPACESHIP = 130;
		public static final int HAUTEUR_SPACESHIP = 130;
		
		// Dimension du bouclier
		public static final int HAUTEUR_SHIELD = 62;
		public static final int LARGEUR_SHIELD = 70;
		
		// Constantes relatives aux météorites
		public static final int LARGEUR_METEOR = 80;
		public static final int HAUTEUR_METEOR = 60;
		public static final int START_POSITION_Y = 0;
		
		// Dimension du background
		public static final int HAUTEUR_BG = 1920;        
        public static final int INCREMENT = 3;
		
		// Vitesse du laser
		public static final int LASER_SPEED = 4;
		
		// Taux de raffraichissement de l'application
		public static final int GAME_SPEED = 7;
		
		// Lien des images utilisés
		
		public static final String BACKGROUND_URL = "images/background.gif";
		public static final String SPACESHIP_URL = "images/spaceship.png";
		public static final String SPACESHIPLEFT_URL = "images/spaceshipleft.png";
		public static final String SPACESHIPRIGHT_URL = "images/spaceshipright.png";
		public static final String SPACESHIPDESTROY_URL = "images/spaceshipexplosion.png";
		public static final String METEOR_URL = "images/meteor.png";
		public static final String FIREMETEOR_URL = "images/fireMeteor.png";
		public static final String ICEMETEOR_URL = "images/iceMeteor.png";
		public static final String ICEBERGMETEOR_URL = "images/icebergMeteor.png";
		public static final String ZIGZAGMETEOR_URL = "images/zzMeteor.gif";
		public static final String LASER_URL = "images/laser.png";
		public static final String SPACESHIPSHIELD_URL = "images/spaceshipshield.png";
		public static final String SPACESHIPSHIELDRIGHT_URL = "images/spaceshipShieldR.png";
		public static final String SPACESHIPSHIELDLEFT_URL = "images/spaceshipShieldL.png";
		public static final String SHIELD_URL = "images/shield.png";
		

		// Message de fin de jeu
		public static final String GAME_OVER = "GAME OVER !";
	
}
