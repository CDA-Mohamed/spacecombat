package fr.afpa.image;

import javax.swing.ImageIcon;

import fr.afpa.constants.Constants;

public class ImageFactory {

	
	public static ImageIcon creerImage (Image image) {
		
		ImageIcon icone = null;
		
		switch(image) {
		
		case METEOR :
			icone = new ImageIcon(Constants.METEOR_URL); break;
			
		case FIREMETEOR :
			icone = new ImageIcon(Constants.FIREMETEOR_URL); break;
			
		case ICEMETEOR :
			icone = new ImageIcon(Constants.ICEMETEOR_URL); break;
			
		case ICEBERGMETEOR :
			icone = new ImageIcon(Constants.ICEBERGMETEOR_URL); break;
			
		case ZIGZAGMETEOR :
			icone = new ImageIcon(Constants.ZIGZAGMETEOR_URL); break;
		
		case SPACESHIP :
			icone = new ImageIcon(Constants.SPACESHIP_URL); break;
			
		case SPACESHIPRIGHT :
			icone = new ImageIcon(Constants.SPACESHIPRIGHT_URL); break;
			
		case SPACESHIPLEFT :
			icone = new ImageIcon(Constants.SPACESHIPLEFT_URL); break;
			
		case SPACESHIPDESTROY :
			icone = new ImageIcon(Constants.SPACESHIPDESTROY_URL); break;
			
		case BACKGROUND :
			icone = new ImageIcon(Constants.BACKGROUND_URL); break;
			
		case LASER :
			icone = new ImageIcon(Constants.LASER_URL); break;
		
		case SHIELD :
			icone = new ImageIcon(Constants.SHIELD_URL); break;
			
		case SPACESHIPSHIELD :
			icone = new ImageIcon(Constants.SPACESHIPSHIELD_URL); break;
			
		case SPACESHIPSHIELDLEFT:
			icone = new ImageIcon(Constants.SPACESHIPSHIELDLEFT_URL); break;
			
		case SPACESHIPSHIELDRIGHT :
			icone = new ImageIcon(Constants.SPACESHIPSHIELDRIGHT_URL); break;
		
		default : return null;	
		}
		return icone;
	}
}
