package fr.afpa.main;

import java.util.ArrayList;

import javax.swing.SwingUtilities;

import fr.afpa.serialisation.ScorePlayeur;
import fr.afpa.serialisation.SerialisationScore;
import fr.afpa.util.GameMainFrame;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
        SwingUtilities.invokeLater(new Runnable()  { 
        	public void run() {
        	new GameMainFrame(); 
        }
    });
    	
}
}
