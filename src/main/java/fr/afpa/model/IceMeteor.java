package fr.afpa.model;

import javax.swing.ImageIcon;

import fr.afpa.image.Image;
import fr.afpa.image.ImageFactory;
import lombok.NoArgsConstructor;
@NoArgsConstructor
public class IceMeteor extends Meteor {
	
	public IceMeteor(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		initialize();
	}
	
	public void move (float direction) {
		this.y += direction;
	}
	
	private void initialize() {
		ImageIcon icone = ImageFactory.creerImage(Image.ICEMETEOR);
		setImage(icone.getImage());
	}
	
}
