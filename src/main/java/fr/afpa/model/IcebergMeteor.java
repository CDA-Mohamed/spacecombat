package fr.afpa.model;

import javax.swing.ImageIcon;

import fr.afpa.image.Image;
import fr.afpa.image.ImageFactory;
import lombok.NoArgsConstructor;

public class IcebergMeteor extends Meteor{
	
	public IcebergMeteor(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		initialize();
	}
	
	public void move (float direction) {
		this.y += direction;
	}
	
	private void initialize() {
		ImageIcon icone = ImageFactory.creerImage(Image.ICEBERGMETEOR);
		setImage(icone.getImage());
	}
	
	}

