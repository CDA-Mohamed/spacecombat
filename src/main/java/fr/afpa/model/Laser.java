package fr.afpa.model;

import javax.swing.ImageIcon;

import fr.afpa.constants.Constants;
import fr.afpa.image.Image;
import fr.afpa.image.ImageFactory;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter 
@NoArgsConstructor

public class Laser extends Sprite {

	public Laser(int x, int y) {
		this.x = x;
		this.y = y;
		initialize();
	}
	
	// Méthode pour initialiser les paramètres de départ du laser
	private void initialize() {
		
		ImageIcon icone = ImageFactory.creerImage(Image.LASER);
		setImage(icone.getImage());
		
		setX(x + Constants.LARGEUR_SPACESHIP/2 - 7);
		setY(y);
		
	
	}

	@Override
	public void move() { 
		
		y -= Constants.LASER_SPEED;
		
		if (this.y < 0) {
			this.die();
		}
		
	}

}
