package fr.afpa.model;

import javax.swing.ImageIcon;

import fr.afpa.constants.Constants;
import fr.afpa.image.Image;
import fr.afpa.image.ImageFactory;
import fr.afpa.util.GamePanel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor


public class Meteor extends Sprite {
	
	public Meteor (int x, int y) { 
		this.x = x;
		this.y = y;
		initialize(); 
	}

	private void initialize() {
		ImageIcon icone = ImageFactory.creerImage(Image.METEOR);
		setImage(icone.getImage());
		
		
	}

	@Override
	public void move() {
		
	}
	
	public void move (float directionY) {
		this.y += directionY;
		
	}

	
	
}
