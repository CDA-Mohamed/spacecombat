package fr.afpa.model;

import javax.swing.ImageIcon;

import fr.afpa.image.Image;
import fr.afpa.image.ImageFactory;

public class Shield extends Sprite {

	public Shield (int x, int y) {
		this.x = x;
		this.y = y;
		initialize();
	}

	private void initialize() {
		ImageIcon icone = ImageFactory.creerImage(Image.SHIELD);
		setImage(icone.getImage());
		
		
	}
	
	@Override
	public void move() {
		
		}
	
	public void move (float directionY) {
			this.y += directionY;
		}
		
	}


