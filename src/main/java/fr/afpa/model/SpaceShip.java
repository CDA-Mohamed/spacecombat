package fr.afpa.model;

import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;

import fr.afpa.constants.Constants;
import fr.afpa.image.Image;
import fr.afpa.image.ImageFactory;
import fr.afpa.util.GamePanel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class SpaceShip extends Sprite {
	private boolean shieldActive = false;
	
	public SpaceShip() { 
		initialize();
	}
	private void initialize() { 
		ImageIcon iconeBouclier = ImageFactory.creerImage(Image.SPACESHIPSHIELD);	
		ImageIcon iconeClassique = ImageFactory.creerImage(Image.SPACESHIP);	
		if(this.shieldActive) {
			setImage(iconeBouclier.getImage());
		}
		else {
			setImage(iconeClassique.getImage());
		}
		int startX = Constants.LARGEUR_FENETRE/2 - Constants.LARGEUR_SPACESHIP/2;
		int startY = Constants.HAUTEUR_FENETRE - 120;
		
		setX(startX);
		setY(startY); 
	}
	

	
	@Override
	public void move() {
		x += dx;
		y += dy;
		
		// Empêche l'avion de dépasser sur la gauche de la fenêtre
		
		if (x < 0) {
			x = 0;
		}
		
		// Empêche l'avion de dépasser sur la droite (on retire la largeur du vaisseau car ses coordonnées commence en haut à gauche
		// du sprite)
		
		if (x > Constants.LARGEUR_FENETRE - Constants.LARGEUR_SPACESHIP) {
			x = Constants.LARGEUR_FENETRE - Constants.LARGEUR_SPACESHIP;
		}
		
		// Empêche le vaisseau de dépasser vers le haut de la fenêtre
		if (y < 0 ) {
			y = 0;
		}
		
		// Empêche le vaisseau de dépasser vers le bas de la fenêtre
		if (y > Constants.HAUTEUR_FENETRE - Constants.HAUTEUR_SPACESHIP) {
			y = Constants.HAUTEUR_FENETRE - Constants.HAUTEUR_SPACESHIP;
		}
		
	}
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		ImageIcon icone = ImageFactory.creerImage(Image.SPACESHIP);
		ImageIcon iconeBouclier = ImageFactory.creerImage(Image.SPACESHIPSHIELD);
		ImageIcon iconeGauche = ImageFactory.creerImage(Image.SPACESHIPLEFT);
		ImageIcon iconeDroit = ImageFactory.creerImage(Image.SPACESHIPRIGHT);
		ImageIcon iconeGaucheBouclier = ImageFactory.creerImage(Image.SPACESHIPSHIELDLEFT);
		ImageIcon iconeDroitBouclier = ImageFactory.creerImage(Image.SPACESHIPSHIELDRIGHT);
		
		// Aller à gauche
		if (key == e.VK_LEFT) {
			dx = -2;
			if(this.shieldActive) {
				setImage(iconeGaucheBouclier.getImage());
			}
			else {setImage(iconeGauche.getImage());
			}
		}
		
		// Aller à droite
		if (key == e.VK_RIGHT) {
			dx = 2;
			if(this.shieldActive) {
				setImage(iconeDroitBouclier.getImage());
			}
			else {setImage(iconeDroit.getImage());
			}
		}
		
		// Aller en haut
		if (key == e.VK_UP) {
			dy = -2;
			if(this.shieldActive) {
				setImage(iconeBouclier.getImage());
			}
			else {
				setImage(icone.getImage());
			}
		}
		
		// Aller en bas
		if (key == e.VK_DOWN) {
			dy = 2;
			if(this.shieldActive) {
				setImage(iconeBouclier.getImage());
			}
			else {
				setImage(icone.getImage());
			}
		}
		
	}
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		ImageIcon icone = ImageFactory.creerImage(Image.SPACESHIP);
		ImageIcon iconeBouclier = ImageFactory.creerImage(Image.SPACESHIPSHIELD);	
		
		// Ne plus aller à gauche en relachant la touche
		if (key == e.VK_LEFT) {
			dx = 0;
			if(this.shieldActive) {
				setImage(iconeBouclier.getImage());
			}
			else {
				setImage(icone.getImage());
			}
		}
		
		// Ne plus aller à doite en relachant la touche
		if (key == e.VK_RIGHT) {
			dx = 0;
			if(this.shieldActive) {
				setImage(iconeBouclier.getImage());
			}
			else {
				setImage(icone.getImage());
			}	
			}
		
		// Ne plus aller en haut en relachant la touche
		if (key == e.VK_UP) {
			dy = 0;
			if(this.shieldActive) {
				setImage(iconeBouclier.getImage());
			}
			else {
				setImage(icone.getImage());
			}	
		}
		
		// Ne plus aller en bas en relachant la touche
		if (key == e.VK_DOWN) {
			dy = 0;
			if(this.shieldActive) {
				setImage(iconeBouclier.getImage());
			}
			else {
				setImage(icone.getImage());
			}	
			
		}
		
	}
	

}
