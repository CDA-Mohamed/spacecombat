package fr.afpa.model;

import java.awt.Image;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter 

public abstract class Sprite {
	
	private Image image;
	private boolean dead;
	protected int x;
	protected int y;
	protected int dx;
	protected int dy;

	public abstract void move(); 
	
	public Sprite() {
		this.dead = false;
	}
	
	public void die() {
		this.dead = true;
	}
	
}
