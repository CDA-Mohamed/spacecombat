package fr.afpa.model;

import javax.swing.ImageIcon;

import fr.afpa.constants.Constants;
import fr.afpa.image.Image;
import fr.afpa.image.ImageFactory;
import fr.afpa.util.GamePanel;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ZigZagMeteor extends Meteor {
	
	public ZigZagMeteor(int x, int y) {
		this.x = x;
		this.y = y;
		initialize();
	}

	
	private void initialize() {
		ImageIcon icone = ImageFactory.creerImage(Image.ZIGZAGMETEOR);
		setImage(icone.getImage());
	}

	@Override
	public void move(float directionY) {
		super.move(directionY);
		this.x = GamePanel.randInt(-100,100);
	}

}