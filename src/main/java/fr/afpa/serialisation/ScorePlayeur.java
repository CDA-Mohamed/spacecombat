package fr.afpa.serialisation;

import java.io.Serializable;
import java.time.LocalDateTime;

import fr.afpa.util.GamePanel;
import fr.afpa.util.StartDialog;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@AllArgsConstructor
/**
 * 
 * @author adrien
 * classe beans permettant la serialisation et inscrire par consequent l affichage des scores nom date
 *
 */

public class ScorePlayeur implements Serializable, Comparable<ScorePlayeur>{

private String nomJoueur;
private int score;
private LocalDateTime date; 
private static final long serialVersionUID = 1L;
public ScorePlayeur(String nomJoueur, int score) { 
	
	this.nomJoueur =nomJoueur;
	this.score = score; 
	this.date =LocalDateTime.now();
}
/**
 * methode permettnat de comparer les scores des differents joueurs (redifinition de methode)
 */
@Override
public int compareTo(ScorePlayeur o) {
	
	return this.getScore()-o.getScore();
}
//pour afficher les scores
@Override
public String toString() {
	return "nom : " + nomJoueur + " score : " + score + " date : " + date ;
}
//pour les methodes de test unitaires
@Override
public boolean equals(Object arg0) {
	ScorePlayeur joueur = (ScorePlayeur)arg0;
	
	return this.nomJoueur.equals(joueur.getNomJoueur())&&this.score==joueur.getScore()&&this.date.equals(joueur.getDate()); 
}



}
