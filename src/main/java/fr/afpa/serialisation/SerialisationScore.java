package fr.afpa.serialisation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
/**
 * 
 * @author adrien
 *classe possedant les differentes methodes de serialisation de 
 */
public class SerialisationScore {

 private final String FILE = "spacer_score_cda20156.ser";
	
 private ArrayList<ScorePlayeur> listeDesScores = new ArrayList<ScorePlayeur>(); 

 public void mettreAjourLaListeDesScores(ScorePlayeur joueur) throws ClassNotFoundException, IOException {
		//verifier si liste des scores existes
		if (fichierExist()) {  
		//si la liste existe 
			
			//la recuperer 
		listeDesScores=recupererListeDesScores();
			//voir s il ya 20 entrees dans la liste
		if (listeDesScores.size()<20) {
			listeDesScores.add(joueur);
		}else {
		// la comparer avec le score du joueur rentrer en param
			
			int	scoreMini = 0;
			int indexScoreMini =0; 
			//voir si > ou < / aux autres joueurs
			
			// modifier la liste si necessaire dans le top 20
				for (int i = 0; i < listeDesScores.size(); i++) {
					if (i==0) {
						scoreMini=listeDesScores.get(i).getScore();
						indexScoreMini=i;
					}else {
				
						if(listeDesScores.get(i).getScore()<scoreMini) {
						scoreMini=listeDesScores.get(i).getScore();
						indexScoreMini=i;
						}	
					}
					
				}
			if (joueur.getScore()>scoreMini) {  
				listeDesScores.set(indexScoreMini, joueur);
			}
		}
			
		}else { 
			listeDesScores.add(joueur);
			// si liste n existe pas
			
				//creer la liste et ajouter le joueur
			
		}
		

		//enregistrer la nouvelle liste 
		ecrireFichierScore(listeDesScores);
		Collections.sort(listeDesScores);
		Collections.reverse(listeDesScores);
		//envoyer la liste
//		for ( ScorePlayeur joueurA : listeDesScores) {
//			System.out.println(" nom : "+joueurA.getNomJoueur()+" score : "+joueurA.getScore()+" date :"+joueurA.getDate());
			
			
		}
		
		
		//return listeDesScores;
		

	public void ecrireFichierScore(ArrayList<ScorePlayeur>listeDeScoreAEcrire) throws IOException {
		 FileOutputStream fos = new FileOutputStream(FILE);
		 ObjectOutputStream oos = new ObjectOutputStream(fos);
		 
		 
		oos.writeObject(listeDeScoreAEcrire);
		
		 
		 	oos.close(); 
		 	
		 	
	 	 }
	public ArrayList<ScorePlayeur>  recupererListeDesScores() throws ClassNotFoundException, IOException {
	
		ArrayList<ScorePlayeur>recupPlayeur=new ArrayList<ScorePlayeur>();
		FileInputStream f = new FileInputStream(FILE);
		ObjectInputStream oos = new ObjectInputStream(f);
		
		 recupPlayeur=(ArrayList<ScorePlayeur>) oos.readObject();
		 oos.close();	
		Collections.sort(recupPlayeur);
		Collections.reverse(recupPlayeur);
		return recupPlayeur;
	}
	public boolean fichierExist() {
		File myFile = new File(FILE);
		return  myFile.exists();
}}
