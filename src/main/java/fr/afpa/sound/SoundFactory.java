package fr.afpa.sound;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.UnsupportedAudioFileException;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class SoundFactory {
	private Clip clip;
	
	public void laserSound() {
		try {
			AudioInputStream sonLaser = AudioSystem.getAudioInputStream(new File("sounds/laser.wav"));
			clip = AudioSystem.getClip();
			clip.open(sonLaser);
			clip.start();
		
		
		}catch(Exception e) {
			e.printStackTrace();
			
		}
	}
	
	public void loopSound(boolean verifGame) {
		AudioInputStream fondSonore;
		if(verifGame==true) {
			try {
				fondSonore = AudioSystem.getAudioInputStream(new File("sounds/loop.wav"));
				clip = AudioSystem.getClip();
				clip.open(fondSonore);
				clip.start();
				clip.loop(Clip.LOOP_CONTINUOUSLY);
				
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		
		} else {
			
				clip.stop();
				clip.close();
			}		
	}
	public void explosionSound() {
		try {
			AudioInputStream sonExplosion = AudioSystem.getAudioInputStream(new File("sounds/explosion.wav"));
			clip = AudioSystem.getClip();
			clip.open(sonExplosion);
			clip.start();
		
		
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void shieldSound() {
		try {
			AudioInputStream sonBouclier = AudioSystem.getAudioInputStream(new File("sounds/shield.wav"));
			clip = AudioSystem.getClip();
			clip.open(sonBouclier);
			clip.start();
		
		
		}catch(Exception e) {
			e.printStackTrace();
			
		}
	}
	
	public void impactSound() {
		try {
			AudioInputStream sonImpact = AudioSystem.getAudioInputStream(new File("sounds/impact.wav"));
			clip = AudioSystem.getClip();
			clip.open(sonImpact);
			clip.start();
		
		
		}catch(Exception e) {
			e.printStackTrace();
			
		}
	}
}
