package fr.afpa.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.sound.sampled.Clip;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import fr.afpa.image.Image;
import fr.afpa.image.ImageFactory;
import fr.afpa.serialisation.ScorePlayeur;
import fr.afpa.serialisation.SerialisationScore;
import fr.afpa.sound.SoundFactory;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Thomas
 * class EndDialog : permet d'afficher une boîte de dialogue à la fin du jeu avec 3 boutons
 */
public class EndDialog extends JDialog implements ActionListener, WindowListener {

	JPanel endGame = new JPanel();
	JLabel insertionNewGame = new JLabel("Nouvelle partie    ");
	JButton newGame = new JButton("GO");
	JLabel insertionBestScore = new JLabel("Meilleurs Scores ");
	JButton bestScore = new JButton("GO");
	JLabel insertionQuit = new JLabel("Quitter le jeu        ");
	JButton quit = new JButton("GO");
	
	JPanel showBestScore = new JPanel();
	JLabel top20 = new JLabel();
	JButton retourEndDialog = new JButton("Retour");
	
	public EndDialog() {
		
	JPanel endGame;
	JPanel showBestScore;
	
	Box boite = Box.createVerticalBox();
	setTitle("                                         Fin de partie");
	
	Font font = new Font ("Helvetica", Font.BOLD, 15);
	
	endGame = new JPanel();
	endGame.add(insertionNewGame);
	insertionNewGame.setBackground(Color.magenta);
	insertionNewGame.setForeground(Color.white);
	insertionNewGame.setFont(font);
	endGame.add(newGame);
	newGame.addActionListener(this);
	endGame.setBackground(Color.magenta);
	boite.add(endGame);
	
	endGame = new JPanel();
	endGame.add(insertionBestScore);
	insertionBestScore.setBackground(Color.magenta);
	insertionBestScore.setForeground(Color.white);
	insertionBestScore.setFont(font);
	endGame.add(bestScore);
	bestScore.addActionListener(this);
	endGame.setBackground(Color.magenta);
	boite.add(endGame);
	
	endGame = new JPanel();
	endGame.add(insertionQuit);
	insertionQuit.setBackground(Color.magenta);
	insertionQuit.setForeground(Color.white);
	insertionQuit.setFont(font);
	endGame.add(quit);
	quit.addActionListener(this);
	endGame.setBackground(Color.magenta);
	boite.add(endGame);
	
	add(boite);
	boite.setBackground (Color.magenta); 
	
	setIconImage(ImageFactory.creerImage(Image.SPACESHIP).getImage());
	setPreferredSize(new Dimension(500,200));
	setLocation(700,400);
	
	addWindowListener(this);
	pack();
	setVisible(true);
	}
	
	/**
	 * @author Thomas
	 * Au clic sur chacun des boutons, récupération du bouton concerné, puis application de la méthode correspondante
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(newGame)){
			Frame[] gmf = GameMainFrame.getFrames().clone();
			ArrayList <Frame> gmf2 = new ArrayList();
			for (int i =0; i < gmf.length; i++) {
				if (gmf[i] != null) {
					gmf2.add(gmf[i]);
				}
				for(Frame f : gmf2) {
					f.dispose();
				}
				gmf2.clear();
				
				
				
			}
			   SwingUtilities.invokeLater(new Runnable()  { 
		        	public void run() {
		        	new GameMainFrame();
		        }
			   });
		
		} else if(e.getSource().equals(quit)){
			System.exit(-2);;
		} else if (e.getSource().equals(bestScore)) {
			this.setVisible(false);
			try {
				TopScoreFrame top = new TopScoreFrame();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	@Override
	public void windowActivated(WindowEvent arg0) {
	}
	
	@Override
	public void windowClosed(WindowEvent arg0) {
	}
	/**
	 * @author Thomas
	 * windowClosing : quand on clique sur la croix de la fenêtre EndDialog, la JDialog devient invisible
	 */
	@Override
	public void windowClosing(WindowEvent arg0) {
		this.setVisible(false);
	}
	
	@Override
	public void windowDeactivated(WindowEvent arg0) {
	}
	
	@Override
	public void windowDeiconified(WindowEvent arg0) {
	}
	
	@Override
	public void windowIconified(WindowEvent arg0) {
	}
	
	@Override
	public void windowOpened(WindowEvent arg0) {
	}
	
}