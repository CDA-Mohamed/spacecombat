package fr.afpa.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class FermetureFrame implements ActionListener {
	private JFrame frame = null;
	
	public FermetureFrame (GameMainFrame f) {
		this.frame = f;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.frame.dispose(); 
		
	}

}
