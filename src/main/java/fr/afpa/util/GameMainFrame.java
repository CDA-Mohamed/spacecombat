package fr.afpa.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import fr.afpa.constants.Constants;
import fr.afpa.image.Image;
import fr.afpa.image.ImageFactory;

public class GameMainFrame extends JFrame implements ActionListener {

	public GameMainFrame() {
		initializeLayout();
	}

	private void initializeLayout() {
		
		add(new GamePanel());
		
		setTitle(Constants.TITRE);
		setIconImage(ImageFactory.creerImage(Image.SPACESHIP).getImage());
		
		pack();
		setResizable(false);
		setVisible(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
	}
	
	
}
