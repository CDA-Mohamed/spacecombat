package fr.afpa.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

import fr.afpa.constants.Constants;
import fr.afpa.controller.GameEventListener;
import fr.afpa.image.Image;
import fr.afpa.image.ImageFactory;
import fr.afpa.model.FireMeteor;
import fr.afpa.model.IceMeteor;
import fr.afpa.model.IcebergMeteor;
import fr.afpa.model.Laser;
import fr.afpa.model.Meteor;
import fr.afpa.model.Shield;
import fr.afpa.model.SpaceShip;
import fr.afpa.model.ZigZagMeteor;
import fr.afpa.serialisation.ScorePlayeur;
import fr.afpa.serialisation.SerialisationScore;
import fr.afpa.sound.SoundFactory;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter


public class GamePanel extends JPanel {
	
	private ImageIcon backgroundImage;
	private SoundFactory soundFactory;
	private SoundFactory bandeSon;
	private static StartDialog startDialog;
	private static EndDialog endDialog;
	private static TopScoreFrame topScoreFrame;
	private Timer timer;
	private SpaceShip spaceShip;
	private Random random;
	private boolean inGame = true;
	private float directionX = 2;
	private float directionY = 2;
	private List<Meteor> meteors;
	private List<Laser> lasers;
	private List<Shield> shields;
	private Meteor meteor;
	private FireMeteor fireMeteor;
	private IcebergMeteor icebergMeteor;
	private ZigZagMeteor zigzagMeteor;
	private IceMeteor iceMeteor;
	private String message;
	private int score;
	private int spaceshipLife = 5;
	
	
	public GamePanel() {
		initializeVariables();
		initializeLayout();
		initializeGame();		
	}

	/**
	 * Méthode servant à initialiser le jeu avec le fond sonore et la première météorite
	 */
	private void initializeGame() {
		this.bandeSon.loopSound(true);

		
		// Générateur de météorites aléatoires permettant de générer la première météorite et de l'ajouter à la liste de météorites
		
		int tirageMeteor = randInt (1,5);
		int tiragePositionX = randInt(80,Constants.LARGEUR_FENETRE-80); 
		
		if (tirageMeteor == 1) {			
			meteor = new Meteor(tiragePositionX, Constants.START_POSITION_Y);
		}
		
		else if (tirageMeteor == 2) { 		
			meteor = new FireMeteor(tiragePositionX, Constants.START_POSITION_Y);
			}
		
		
		else if (tirageMeteor == 3) {
			meteor = new IceMeteor(tiragePositionX, Constants.START_POSITION_Y);
			}
		
		
		else if (tirageMeteor == 4) {
			meteor = new IcebergMeteor(tiragePositionX, Constants.START_POSITION_Y);
		}
		
		else if (tirageMeteor == 5) {			
			meteor = new ZigZagMeteor(tiragePositionX, Constants.START_POSITION_Y);
			}
			if (this.meteors.size() > 4) {
				this.meteors.add(meteor);
			}
			else {}
		
	}
	
/**
 * Methode d'initialisation des objets utilisés dans la classe
 */
	private void initializeVariables() {
		this.startDialog = new StartDialog();
		this.meteors = new ArrayList <Meteor>();
		this.shields = new ArrayList<Shield>();
		this.meteor = new Meteor();
		this.random = new Random();
		this.lasers = new ArrayList<Laser>();
		this.spaceShip = new SpaceShip();
		this.soundFactory = new SoundFactory();
		this.bandeSon = new SoundFactory();
		this.backgroundImage = ImageFactory.creerImage(Image.BACKGROUND);
		this.timer = new Timer(Constants.GAME_SPEED, new GameLoop(this));
		this.timer.start();
	}

	/**
	 * Méthode d'initialisation des paramètres de fenêtre du Panel
	 */
	private void initializeLayout() {
		addKeyListener(new GameEventListener(this));
		setFocusable(true);
		setPreferredSize(new Dimension(Constants.LARGEUR_FENETRE, Constants.HAUTEUR_FENETRE));	
	}
	
	
 
	
	// Méthode servant à dessiner le vaisseau
	private void drawSpaceShip(Graphics g) {
		g.drawImage(spaceShip.getImage(), spaceShip.getX(),spaceShip.getY(),this);
	}
	
	// Méthode servant à dessiner le laser s'il n'est pas "mort"
	private void drawLaser(Graphics g) {
		for (Laser laser : lasers)
			if(!laser.isDead()) {
				g.drawImage(laser.getImage(),laser.getX(),laser.getY(),this);
			}
		}
	
	
	// Méthode servant à dessiner les météores 
	private void drawMeteors (Graphics g) {	
		for (Meteor meteor : meteors)
			if (!meteor.isDead()) {
				g.drawImage(meteor.getImage(),meteor.getX(),meteor.getY(),this);
			
			}		
		}
	
	
	private void drawShields(Graphics g) {
		for (Shield shield : shields)
			if(!shield.isDead()) {
				g.drawImage(shield.getImage(),shield.getX(),shield.getY(),this);
			}
		}
	
	
	// Méthode servant à affichant le message de fin de jeu
	private void drawGameOver(Graphics g) {
		g.drawImage(backgroundImage.getImage(),0,0,null);
		
		Font font = new Font ("Helvetica", Font.BOLD, 50);
		FontMetrics fontMetrics = this.getFontMetrics(font);
		
		g.setColor(Color.WHITE);
		g.setFont(font);
		g.drawString(Constants.GAME_OVER, (Constants.LARGEUR_FENETRE - fontMetrics.stringWidth(message)) / 2, 200);
		

	}
	
	
	/**
	 * Méthode servant à dessiner et afficher le score et les points de vie du vaisseau
	 * @param g objet de la classe Graphics
	 */
	private void drawScore(Graphics g) {
			
		if(!inGame) {
			
			return;
		}
		
		Font font = new Font ("Helvetica", Font.BOLD, 20);
		g.setColor(Color.WHITE);
		g.setFont(font);
		g.drawString("SCORE : " + score, 50,50);
		g.drawString("LIFE : " + spaceshipLife ,Constants.LARGEUR_FENETRE - 150, 50);
		g.drawString(startDialog.getChampNom().getText(), Constants.LARGEUR_FENETRE/2 - 3 ,50);
	}
	
	
	@Override
	protected void paintComponent(Graphics g) {	
		super.paintComponent(g);		
		g.drawImage(backgroundImage.getImage(),0,0,null);
		doDrawing(g);
	}
	


	/**
	 * Méthode servant à dessiner les différents éléments graphiques de la fenêtre lorsque le jeu est en cours
	 * @param g : objet de la classe Graphics permettant de mettre en forme les différents éléments 
	 */
	private void doDrawing(Graphics g) {
		if (inGame) {
		drawScore(g);
		drawSpaceShip(g);
		drawLaser(g);
		drawMeteors(g);
		drawShields(g);
		}
		else {
			if(timer.isRunning()) {
				timer.stop();
				this.bandeSon.loopSound(false);
				
				drawGameOver(g);
				drawSpaceShip(g);
				EndDialog ed = new EndDialog();			
				
			}
			
		// Lorsque le jeu est terminée, on affiche uniquement le message de fin ainsi que le vaisseau	
			
		}
		
		Toolkit.getDefaultToolkit().sync();
	}

	
/**
 *  Méthode de boucle qui enchaine entre la mise à jour des éléments graphiques et le raffraichissement 
 */
	public void doOneLoop() {
		update();
		repaint();
	}

	/**
	 * Méthode servant à mettre à jour les différents éléments graphiques en fonction des événements qui ont lieu ou des interractions entre elles
	 */
	private void update() {
				
			if (!spaceShip.isDead()) {
				Meteor destroyedMeteor = null;
				for (Meteor meteor : this.meteors) {
				int meteorX = meteor.getX();
				int meteorY = meteor.getY();
				
				int spaceshipX = spaceShip.getX();
				int spaceshipY = spaceShip.getY();
				
				// Algorithme de detection de collision entre les météores et le vaisseau : si une météorite touche le vaisseau, elle est
				//supprimée de la liste et le vaisseau perd un point de vie
								
				if (meteorX + Constants.LARGEUR_METEOR/2 >= spaceshipX && meteorX + Constants.LARGEUR_METEOR/2 <= (spaceshipX + Constants.LARGEUR_SPACESHIP) 
						&& meteorY + Constants.HAUTEUR_METEOR/2 >= spaceshipY && meteorY + Constants.HAUTEUR_METEOR/2 <=  spaceshipY + Constants.HAUTEUR_SPACESHIP) {
					destroyedMeteor = meteor;
					soundFactory.impactSound();
					if(spaceShip.isShieldActive()) {
						spaceShip.setShieldActive(false);
						ImageIcon icone = ImageFactory.creerImage(Image.SPACESHIP);
						spaceShip.setImage(icone.getImage());
					}
					else {
						if(meteor instanceof IceMeteor){
							spaceshipLife -= 2;
						}else if (meteor instanceof IcebergMeteor) {
							spaceshipLife -=4;
						}else if(meteor instanceof ZigZagMeteor) {
							spaceshipLife -=2;
						}else if(meteor instanceof FireMeteor) {
							spaceshipLife -= 2;
						}else if(meteor instanceof Meteor) {
							spaceshipLife -=1;
						}
					}
				}
				
				// Lorsque le vaisseau n'a plus de point de vie, il est détruit
						if (spaceshipLife <= 0) {
							ImageIcon icone = ImageFactory.creerImage(Image.SPACESHIPDESTROY);
							spaceShip.setImage(icone.getImage());
							soundFactory.explosionSound();
							spaceShip.die();
							
						}
					
				}
				this.meteors.remove(destroyedMeteor);
			Shield shieldUsed = null;
				for (Shield shield : shields) {
					
					int spaceshipX = spaceShip.getX();
					int spaceshipY = spaceShip.getY();
					int shieldX = shield.getX();
					int shieldY = shield.getY();
					if (shieldY < Constants.HAUTEUR_FENETRE) {
						shield.move(directionY);
					} else {
						shield.die();
					}
					
					if (shieldX + Constants.LARGEUR_SHIELD/2 >= spaceshipX && shieldX + Constants.LARGEUR_SHIELD/2 <= (spaceshipX + Constants.LARGEUR_SPACESHIP) 
							&& shieldY + Constants.HAUTEUR_SHIELD/2 >= spaceshipY && shieldY + Constants.HAUTEUR_SHIELD/2 <=  spaceshipY + Constants.HAUTEUR_SPACESHIP) {
						soundFactory.shieldSound();
						shieldUsed = shield;
						spaceShip.setShieldActive(true);
						ImageIcon iconeBouclier = ImageFactory.creerImage(Image.SPACESHIPSHIELD);
						spaceShip.setImage(iconeBouclier.getImage());
					}
					
					
				}
				
				this.shields.remove(shieldUsed);
				
				
				spaceShip.move();
			}
			
			// Lorsque le vaisseau est détruit, le jeu se termine et l'affichage du message de fin s'affiche
			//La liste des scores se met a jour
			if(spaceShip.isDead()) {
				inGame = false;
				message = Constants.GAME_OVER; 
				SerialisationScore scoreSerial = new SerialisationScore();
				ScorePlayeur joueur = new ScorePlayeur(startDialog.getChampNom().getText(),this.score);
				
				try {
					scoreSerial.mettreAjourLaListeDesScores(joueur);
					
					
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
			}
			
			
			// Tant que la météorite n'est pas morte, elle reste en mouvement en fonction de sa vitesse obtenue par la variable "direction"
			Meteor destroyedMeteor = null;
			for (Meteor meteor : this.meteors) {
				if(!meteor.isDead()) {
					
					
					meteor.move(directionY);}
			
			
			// A chaque fois qu'une météorite atteint le bas de l'écran, elle meurt et le score augmente jusqu'à atteindre 999 au maximum
			
			
					if(meteor.getY() >= Constants.HAUTEUR_FENETRE) {
						
						
						if(score<999) {
						
						if(meteor instanceof IceMeteor){
							score += 3;
						}else if (meteor instanceof IcebergMeteor) {
							score +=5;
						}else if(meteor instanceof ZigZagMeteor) {
							score +=5;
						}else if(meteor instanceof FireMeteor) {
							score += 3;
						}else if(meteor instanceof Meteor) {
							score+=2;
						}
					}
						destroyedMeteor = meteor;
						
						
				}
			
			}
			this.meteors.remove(destroyedMeteor);
			
	
		
			// Générateur aléatoire de météorites qui va créer une météorite de type aléatoire et de position aléatoire
			
		if(random.nextDouble() < 0.005) {
			
				int tirageMeteor = randInt (1,5);
				int tiragePositionX = randInt(50,Constants.LARGEUR_FENETRE-50);
				
				if (tirageMeteor == 1) {
					meteor = new Meteor(tiragePositionX, Constants.START_POSITION_Y);
					}

				
				else if (tirageMeteor == 2) {
					meteor = new FireMeteor(tiragePositionX, Constants.START_POSITION_Y);
					}
		
				
				else if (tirageMeteor == 3) {
					meteor = new IceMeteor(tiragePositionX, Constants.START_POSITION_Y);
					}
				
				else if (tirageMeteor == 4) {
					meteor = new IcebergMeteor(tiragePositionX, Constants.START_POSITION_Y);
				}
				
				else if (tirageMeteor == 5) {
					meteor = new ZigZagMeteor(tiragePositionX, Constants.START_POSITION_Y);
				}
			this.meteors.add(meteor);

			}
		
		// / Générateur aléatoire de boucliers  qui va générer un bouclier à une position aléatoire
		
		if(random.nextDouble() < 0.001) {
			int tiragePositionX = randInt(50,Constants.LARGEUR_FENETRE-50);
			
			Shield shield = new Shield(tiragePositionX,Constants.START_POSITION_Y);
			
			this.shields.add(shield);
			}
		
		
		
		// Augmente la vitesse des météorites à chaque palier de 100 points
		if (score % 100 == 0 && score != 0) {
			directionY+= 0.01;
		}
		
		// On instancie un objet destroyedMeteor pour qu'à chaque fois qu'un météore est détruit, on puisse l'effacer de la liste de météores
		// On fait de même pour les lasers
		destroyedMeteor = null;
		Laser destroyedLaser = null;
		
		for (Laser laser : this.lasers) {
			if (!laser.isDead()) {
				
			for(Meteor meteor : this.meteors) {
				
			int shotX = laser.getX();
			int shotY = laser.getY();
			
			int meteorX = meteor.getX();
			int meteorY = meteor.getY();
			
			
			// Algorithme de detection de collision entre le laser et les météores : si un laser touche un météore, les 2 sont détruits et
			// on les supprime de leurs listes respectives 
			if (shotX >= meteorX && shotX <= (meteorX + Constants.LARGEUR_METEOR) && shotY >= meteorY && shotY <=
						meteorY + Constants.HAUTEUR_METEOR) {
					destroyedMeteor = meteor;
					destroyedLaser = laser;
					soundFactory.explosionSound();
					
					// Bloque le score a 999
					if (score >= 999) {
						score = 999;
					}
					
					// Ajoute 5 au score lorsque une météorite est détruite par un laser
					else {
					score += 5;
					}
				}
			
			}			
			this.meteors.remove(destroyedMeteor);
			}
		}
		this.lasers.remove(destroyedLaser);
		
		
		// Pour chaque laser dépassant le bas de la fenêtre, on le supprime de la liste des lasers sinon on le fait se déplacer
		for (Laser laser : this.lasers) {
			if(laser.getY()<0) {
				destroyedLaser = laser;
			} else {
				laser.move();
			}
		}
		
		this.lasers.remove(destroyedLaser);

	}
	
	/**
	 * @param e L'évenement correspond à l'enfoncement de la barre d'espace
	 * La méthode crée un laser qui démarre à la position du vaisseau et l'ajoute à la liste de lasers
	 */
	public void keyPressed(KeyEvent e) {
		this.spaceShip.keyPressed(e);
		
		int key = e.getKeyCode();
		
		if (key == KeyEvent.VK_SPACE) {
			
			int laserX = this.spaceShip.getX();
			int laserY = this.spaceShip.getY();
			
			if(inGame) {
				
				soundFactory.laserSound();
				lasers.add(new Laser(laserX, laserY));
			}
		}
		
	}

	public void keyReleased(KeyEvent e) {
		this.spaceShip.keyReleased(e);		
	}
	
	public static int randInt(int min, int max) {
		if (min >= max) {
			System.out.println("max doit etre superieur de la valeur min");
		}
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
	

}

