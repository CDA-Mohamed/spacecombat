package fr.afpa.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fr.afpa.image.Image;
import fr.afpa.image.ImageFactory;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class StartDialog extends JDialog implements ActionListener, WindowListener{
	    
	    JButton valider = new JButton("Nouvelle Partie");
	    JTextField champNom = new JTextField(10);
	    
	    JLabel insertionNom = new JLabel("Entrez votre nom  : ");
	    JLabel errorMessage = new JLabel("Votre nom est invalide, veuillez réessayer.");
	    
	    public StartDialog() {
		JPanel panneau ;
		
		Box boite = Box.createVerticalBox();
		setModal(true);
		setTitle("                        Bienvenue dans Space Combat !");
		
		Font font = new Font ("Helvetica", Font.BOLD, 15);
		
		panneau = new JPanel();
		panneau.add(errorMessage);
		errorMessage.setVisible(false);
		errorMessage.setForeground(Color.RED);
		errorMessage.setFont(font);
		panneau.setBackground (Color.magenta);
		boite.add(panneau);
		
		
		
		panneau = new JPanel();
		panneau.add(insertionNom);
		insertionNom.setForeground(Color.white);
		insertionNom.setFont(font);
		panneau.add(champNom);
		panneau.setBackground (Color.magenta);
		boite.add(panneau);
				
		panneau = new JPanel();
		panneau.add(valider);
		valider.setBackground(Color.WHITE);
		panneau.setBackground (Color.magenta);
		boite.add(panneau);
		
		add(boite);
		boite.setBackground (Color.magenta); 
		
		setIconImage(ImageFactory.creerImage(Image.SPACESHIP).getImage());
		setPreferredSize(new Dimension(500,200));
		setLocation(700,400);
		
		getRootPane ().setOpaque (false);
		getContentPane ().setBackground (Color.magenta);
		setBackground (Color.magenta);
		
		
		
		addWindowListener(this);
		valider.addActionListener(this);
		pack();
		setVisible(true);
	    }
	    
	    public void actionPerformed(ActionEvent evt) {
	    	
		    	 if (!this.controlName(champNom.getText())) {
		    		 errorMessage.setVisible(true);
		    		
		    	 }
		    	 else {errorMessage.setVisible(false);
		    	 
		    	 this.setVisible(false);
		    	 }
		     }
	    
	    
	    public boolean controlName(String name) {
			if ((name.length() >= 3 && name.length() <= 6) && !name.contains(";")) {
				return true;
			}
			return false;
		}

		@Override
		public void windowActivated(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosed(WindowEvent arg0) {
		
			
		}

		@Override
		public void windowClosing(WindowEvent arg0) {
			System.exit(0);
			
		}

		@Override
		public void windowDeactivated(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeiconified(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowOpened(WindowEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	}
