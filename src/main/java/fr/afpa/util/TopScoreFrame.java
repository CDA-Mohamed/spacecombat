package fr.afpa.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

import fr.afpa.image.Image;
import fr.afpa.image.ImageFactory;
import fr.afpa.serialisation.ScorePlayeur;
import fr.afpa.serialisation.SerialisationScore;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter


/**
 * @author Thomas
 * Frame qui affiche les 20 meilleurs scores
 */
public class TopScoreFrame extends JFrame implements WindowListener, ActionListener{

	JLabel titre = new JLabel("Voici les 20 meilleurs scores :");
	JList <ScorePlayeur> top20 = new JList();
	DefaultListModel<ScorePlayeur> theTop20 = new DefaultListModel<ScorePlayeur>();
	JButton retour = new JButton("Retour");
	
        public TopScoreFrame() throws ClassNotFoundException, IOException {
        	
        	JPanel contenu;
        	
        	Box boite = Box.createVerticalBox();
        	setTitle("                          Meilleurs scores de tous les temps");
        	
        	Font font = new Font ("Helvetica", Font.BOLD, 15);
        	
        	contenu = new JPanel();
        	contenu.add(titre);
        	titre.setForeground(Color.white);
        	titre.setFont(font);
        	contenu.setBackground (Color.magenta);
        	boite.add(contenu);
        	
        	contenu = new JPanel();
        	contenu.add(implementerScoreList());
        	boite.add(contenu);
        	
        	contenu = new JPanel();
        	contenu.add(retour);
            retour.addActionListener(this);
            contenu.setBackground(Color.MAGENTA);
        	boite.add(contenu);
        	
        	add(boite);
        	boite.setBackground(Color.MAGENTA);
        	
        	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
    		setLocation((screen.width - this.getSize().width)/2,(screen.height - this.getSize().height)/2);
        	
        	setLocation(800,400);
        	setResizable(false);
        	setDefaultCloseOperation(EXIT_ON_CLOSE);
        	setIconImage(ImageFactory.creerImage(Image.SPACESHIP).getImage());
        	addWindowListener(this);        
        	setVisible(true);
        	//setLocationRelativeTo(null);
        	
        	pack();
        	
        }
        
        
        /**
         * @author Thomas
         * Méthode qui permet d'implémenter la JList top20 (avec les 20 meilleurs joueurs) 
         * @return JList top20
         * @throws ClassNotFoundException
         * @throws IOException
         */
        public JList implementerScoreList() throws ClassNotFoundException, IOException {
        	
        SerialisationScore score = new SerialisationScore();
        	
        	ArrayList listeScore  = new ArrayList(score.recupererListeDesScores());
        	Collections.sort(listeScore);
    		Collections.reverse(listeScore);
        	for (int i = 0; i < listeScore.size() ; i++) {
        		theTop20.addElement((ScorePlayeur) listeScore.get(i));
        		top20.setModel(theTop20);
        	}
        
        	top20.setVisibleRowCount(20);     	
        	top20.setSelectionBackground(Color.MAGENTA);
        	
        	return top20;
        }

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if ( ((JButton)e.getSource()).getText().equals("Retour")){
				EndDialog end;
				end = new EndDialog();
				//System.exit(-4);
				this.setVisible(false);
			}
		}

		@Override
		public void windowActivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosed(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosing(WindowEvent e) {
			// TODO Auto-generated method stub
			this.setVisible(false);
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowOpened(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}
}