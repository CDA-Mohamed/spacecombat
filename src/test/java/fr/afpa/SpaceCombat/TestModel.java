/**
 * 
 */
package fr.afpa.SpaceCombat;

import fr.afpa.constants.Constants;
import fr.afpa.model.Meteor;
import fr.afpa.model.SpaceShip;
import fr.afpa.model.Sprite;
import junit.framework.TestCase;

/**
 * @author adrien
 *
 */
public class TestModel extends TestCase {
	private Meteor meteor;
	private SpaceShip spaceShip;
	private boolean verif;
	
	
	/**
	 * @param name
	 */
	public TestModel(String name) {
		super(name);
		 meteor = new Meteor(0,0);
		 spaceShip = new SpaceShip(); 
		 verif = true; 
	}

	protected void setUp() throws Exception {
		super.setUp();
	}
	//test meteor mouvement
	public void testMoveMeteor() {
		
		meteor.move(5);
		assertNotNull(meteor);
		assertEquals(5, meteor.getY());
	}

	
	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
