/**
 * 
 */
package fr.afpa.SpaceCombat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import fr.afpa.serialisation.ScorePlayeur;
import fr.afpa.serialisation.SerialisationScore;
import fr.afpa.util.GamePanel;
import junit.framework.TestCase;

/**
 * @author adrien
 *test sur les methodes de la  serialisation 
 */
public class TestSerialisation extends TestCase {
//-------------------Partie Test Serialisation---------------------------
		private File fichier;
		private SerialisationScore serialisation;
		private ArrayList<ScorePlayeur>listeDesScores;
		private ScorePlayeur playeur;
		private ScorePlayeur playeurDeux;
//------------------Partie test GamePanel---------------------------------
		private GamePanel gamePanel;
	/**
	 * @param name
	 */
	public TestSerialisation(String name) {
		super(name);
		
		
	}

	protected void setUp() throws Exception {
		super.setUp();
//------------------Partie TestSerialisation---------------------------		
		serialisation = new SerialisationScore();
		fichier=new File("C:\\ENV\\workspace\\spacecombat\\spacer_score_cda20156.ser");
		listeDesScores=new ArrayList<ScorePlayeur>();
		playeur = new ScorePlayeur("maurer", 100);
		playeurDeux = new ScorePlayeur("mimi", 500); 

	}

//----------------Methodes Test sur la serialisation---------------	
	public void testFichierExist() {
		boolean verif =serialisation.fichierExist();
		assertTrue("Le fichier existe", verif);
	}
	
	public void testEcrireFichierScore()  {
	listeDesScores.add(playeur);
	try {
		serialisation.ecrireFichierScore(listeDesScores);
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	ArrayList<ScorePlayeur> listSc = null ;
	try {
		 listSc =  serialisation.recupererListeDesScores();
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	assertNotNull(listSc);
	assertEquals(listSc.size(), 1);
	assertEquals(listSc.get(0), playeur);
	
	
}
	public void testRecupererLaListeDesScores()  {
		ArrayList<ScorePlayeur>recupListesDesScores=new ArrayList<ScorePlayeur>();
		listeDesScores.add(playeur);
		
		try {
			serialisation.ecrireFichierScore(listeDesScores);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			recupListesDesScores=serialisation.recupererListeDesScores();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertNotNull(recupListesDesScores);
		assertEquals(1,recupListesDesScores.size());
		assertEquals(recupListesDesScores.get(0), playeur);
		
	}
	
	public void testMettreAjourLaListeDesScores()  {
		listeDesScores.add(playeur);
		ArrayList<ScorePlayeur>recupScores=new ArrayList<ScorePlayeur>();
		try {
			serialisation.ecrireFichierScore(listeDesScores);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	try {
		serialisation.mettreAjourLaListeDesScores(playeurDeux);
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	try {
		recupScores= serialisation.recupererListeDesScores();
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	assertNotNull(recupScores); 
	assertEquals(2, recupScores.size()); 
	
	assertEquals(recupScores.get(0), playeurDeux);
}


	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
